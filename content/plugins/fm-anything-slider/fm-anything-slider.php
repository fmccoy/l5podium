<?php

/*
Plugin Name: Anything Slider
Plugin URI: http://www.bitbucket.org
Description: jQuery Slider
Version: 0.1
Author: Frank McCoy
Author URI: http://www.whatsyouranthem.com 
*/

/**
 * Copyright (c) 2013 Frank McCoy. All rights reserved.
 *
 * Released under the GPL license
 * http://www.opensource.org/licenses/gpl-license.php
 *
 * This is an add-on for WordPress
 * http://wordpress.org/
 *
 * **********************************************************************
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * **********************************************************************
*/

/*=================================================
 *  Constants
 *================================================*/
if ( ! defined( 'FMAS_BASE_FILE' ) )
  define( 'FMAS_BASE_FILE', __FILE__ );
if ( ! defined( 'FMAS_BASE_DIR' ) )
  define( 'FMAS_BASE_DIR', dirname( FMAS_BASE_FILE ) );
if ( ! defined( 'FMAS_PLUGIN_URL' ) )
  define( 'FMAS_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
  
/**
 * Create Custom Post Type
 *
 * @return void
 * @author Frank McCoy
 */
function fm_anything_slider_setup()
{
  $labels = array(
     'name' => 'Slides',
     'singular_name' => 'Slide',
     'add_new' => 'Add New',
     'add_new_item' => 'Add New Slide',
     'edit_item' => 'Edit Slide',
     'new_item' => 'New Slide',
     'all_items' => 'All Slides',
     'view_item' => 'View Slide',
     'search_items' => 'Search Slides',
     'not_found' =>  'No slides found',
     'not_found_in_trash' => 'No slides found in Trash', 
     'parent_item_colon' => '',
     'menu_name' => 'Slides',
   );

   $args = array(
     'labels' => $labels,
     'public' => true,
     'publicly_queryable' => true,
     'show_ui' => true, 
     'show_in_menu' => true, 
     'query_var' => true,
     'rewrite' => array( 'slug' => 'slides' ),
     'capability_type' => 'post',
     'has_archive' => true, 
     'hierarchical' => true,
     'menu_position' => null,
     'supports' => array( 'title', 'editor', 'post-formats', 'page-attributes' ),
     'register_meta_box_cb' => 'fm_anything_slider_meta_box_add'
   );

   register_post_type( 'slides', $args );
}
add_action( 'init', 'fm_anything_slider_setup' );

/**
 * Meta Box Callback
 *
 * @todo
 * @return void
 * @author Frank McCoy
 */
function fm_anything_slider_meta_box_add()
{
  //
}

/**
 * Slider Shortcode
 *
 * @return void
 * @author Frank McCoy
 */
function fm_anything_slider_shortcode()
{
  wp_enqueue_style('anything-slider-style', FMAS_PLUGIN_URL . 'css/anything.css' );
  wp_enqueue_script('anything-slider', FMAS_PLUGIN_URL . 'bower_components/anything-slider/js/jquery.anythingslider.min.js', array('jquery') );
  wp_enqueue_script('anything-slider-fx', FMAS_PLUGIN_URL . 'bower_components/anything-slider/js/jquery.anythingslider.fx.js' );
  wp_enqueue_script('anything-slider-video', FMAS_PLUGIN_URL . 'bower_components/anything-slider/js/jquery.anythingslider.video.js' );
  wp_enqueue_script('jquery-easing', FMAS_PLUGIN_URL . 'bower_components/anything-slider/js/jquery.easing.1.2.js' );
  wp_enqueue_script('anything-slider-swf', FMAS_PLUGIN_URL . 'bower_components/anything-slider/js/swfobject.js' );
  
  wp_enqueue_script('anything-slider-main', FMAS_PLUGIN_URL . 'js/anythingslider.js' );
  
  ob_start();
  fm_anything_slider_get_slides();
  $html = ob_get_clean(  );
  return $html;
}
add_shortcode('fmas', 'fm_anything_slider_shortcode' );

function fm_anything_slider_get_slides()
{
  //echo 'Anything Slider!';
  global $post;
  
  $args = array( 'post_type' => 'slides', 'order' => 'ASC' );
  
  $query = new WP_Query($args);
  
  if( $query->have_posts() ) : 
    
    echo '<ul id="catalog-slider-01" class="catalog-slider">';
      
    while( $query->have_posts() ) :
       
      $query->the_post();
      
      echo '<li class="panel' . $post->ID . '">' . $post->post_content . '</li>';
      
    endwhile;
    
    echo '</ul>';
    
    wp_reset_postdata();
  else :
    
    echo 'No Slides';
    
  endif;
  
}







